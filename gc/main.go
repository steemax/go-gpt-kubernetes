package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"
	"time"
)

type helmRelease struct {
	Name       string `json:"name"`
	Revision   string `json:"revision"`
	Updated    string `json:"updated"`
	Status     string `json:"status"`
	Chart      string `json:"chart"`
	AppVersion string `json:"app_version"`
	Namespace  string `json:"namespace"`
}

type Config struct {
	Namespace string `json:"namespace"`
	MaxAge    int    `json:"max_age"`
}

func main() {
	// Read the configuration file
	configData, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic(err)
	}

	// Unmarshal the configuration data
	var config Config
	if err := json.Unmarshal(configData, &config); err != nil {
		panic(err)
	}

	currentTime := time.Now()

	out, err := exec.Command("helm", "list", "--namespace", config.Namespace, "--output", "json").Output()
	if err != nil {
		panic(err)
	}

	var releases []helmRelease
	if err := json.Unmarshal(out, &releases); err != nil {
		panic(err)
	}

	var releasesToDelete []string
	// iterate over releases, filter by name and age
	for _, release := range releases {
		if strings.Contains(release.Name, "gateway-feat") {
			updatedTime, err := time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", release.Updated)
			if err != nil {
				panic(err)
			}
			MaxAge := time.Duration(config.MaxAge) * time.Hour
			if currentTime.Sub(updatedTime) > MaxAge {
				releaseName := strings.TrimPrefix(release.Name, "gateway-")
				releasesToDelete = append(releasesToDelete, releaseName)
			}
		}
	}

	// iterate over release names and search for matching releases
	for _, releaseName := range releasesToDelete {
		cmd := exec.Command("helm", "list", "-n", config.Namespace, "--filter", releaseName, "--output", "json")
		out, err := cmd.Output()
		if err != nil {
			fmt.Println("Error running command:", err)
			fmt.Println("Output:", string(out))
			continue
		}

		var releases []helmRelease
		err = json.Unmarshal(out, &releases)
		if err != nil {
			panic(err)
		}

		// print the list of matching releases
		for _, release := range releases {
			fmt.Printf("Deleting release %s (namespace %s)\n", release.Name, release.Namespace)
			cmd := exec.Command("helm", "uninstall", release.Name, "-n", config.Namespace)
			var out bytes.Buffer
			cmd.Stdout = &out

			if err := cmd.Run(); err != nil {
				fmt.Println("Error executing command:", err)
				continue
			}

			fmt.Println("Command output:", string(out.Bytes()))
		}
	}
}
