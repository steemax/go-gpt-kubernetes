package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	admissionv1 "k8s.io/api/admission/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"

	v1alpha1 "github.com/traefik/traefik/v2/pkg/provider/kubernetes/crd/traefik/v1alpha1"
)

var (
	runtimeScheme = runtime.NewScheme()
	codecs        = serializer.NewCodecFactory(runtimeScheme)
	deserializer  = codecs.UniversalDeserializer()
)

func mutateHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "failed to read request body", http.StatusBadRequest)
		return
	}

	var admissionReview admissionv1.AdmissionReview
	if _, _, err := deserializer.Decode(body, nil, &admissionReview); err != nil {
		http.Error(w, "failed to decode admission review", http.StatusBadRequest)
		return
	}

	var traefikService v1alpha1.TraefikService
	if err := json.Unmarshal(admissionReview.Request.Object.Raw, &traefikService); err != nil {
		http.Error(w, "failed to unmarshal traefik service", http.StatusBadRequest)
		return
	}

	if traefikService.Annotations["h2c"] == "true" {
		for i := range traefikService.Spec.Weighted.Services {
			traefikService.Spec.Weighted.Services[i].Scheme = "h2c"
		}
	}

	patch, err := json.Marshal([]map[string]interface{}{
		{
			"op":    "replace",
			"path":  "/spec",
			"value": traefikService.Spec,
		},
	})

	if err != nil {
		http.Error(w, "failed to create patch", http.StatusInternalServerError)
		return
	}

	admissionReview.Response = &admissionv1.AdmissionResponse{
		UID:     admissionReview.Request.UID,
		Allowed: true,
		Patch:   patch,
		PatchType: func() *admissionv1.PatchType {
			pt := admissionv1.PatchTypeJSONPatch
			return &pt
		}(),
	}

	respBytes, err := json.Marshal(admissionReview)
	if err != nil {
		http.Error(w, "failed to marshal admission review", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(respBytes)
}

func main() {
	http.HandleFunc("/mutate", mutateHandler)

	if err := http.ListenAndServeTLS(":443", "/etc/webhook/certs/tls.crt", "/etc/webhook/certs/tls.key", nil); err != nil {
		panic(err)
	}
}

